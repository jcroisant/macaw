# macaw

Efficient color types and math for CHICKEN Scheme.

- **Version:**      0.1.0 (2020-04-20)
- **Project:**      https://gitlab.com/jcroisant/macaw
- **Issues:**       https://gitlab.com/jcroisant/macaw/issues
- **License:**      [BSD 2-Clause](LICENSE.txt)
- **Docs:**         https://wiki.call-cc.org/eggref/5/macaw
- **Maintainer:**   John Croisant (john (at) croisant (dot) net)


## Synopsis

Macaw provides efficient color types, math operations,
and color space conversion. It is primarily meant for computer graphics,
data visualization, image processing, games, etc.
For a more scientific library, see the
[color egg](https://wiki.call-cc.org/eggref/5/color).

Macaw provides a variety of arithmetic, blending, and compositing operations
using linear RGB (floats), perceptual sRGB (bytes), and perceptual HSL (floats).
More operations and color types may be added in the future.

Color objects can either be self-contained for convenience,
or can point to locations in memory to efficiently
work with large blocks of image data.
Low-level operations are also provided which directly modify memory,
without needing to create color objects.


## Code of Conduct

Please be aware that all project participants are expected to abide by
the [Code of Conduct](CODE_OF_CONDUCT.md).
We are committed to making participation in this project a welcoming
and harassment-free experience.
