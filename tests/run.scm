
(import scheme)
(cond-expand
 (chicken-4
  (import chicken)
  (use lolevel
       srfi-1
       srfi-4
       test
       macaw))
 (else
  (import (chicken blob)
          (chicken format)
          (chicken locative)
          (chicken memory)
          (srfi 1)
          (srfi 4)
          test
          macaw)))

(: same? (any any --> boolean))
(define (same? a b)
  (cond
   ((and (pair? a) (pair? b))
    (and (same? (car a) (car b))
         (same? (cdr a) (cdr b))))
   ((and (number? a) (inexact? a)
         (number? b) (inexact? b))
    (near? a b))
   ((and (string? a) (string? b))
    (string=? a b))
   (else (equal? a b))))

(: near? (number number --> boolean))
(define (near? a b #!optional (e 1e-6))
  (< (abs (- a b)) e))


(test-group "color?"
  (test-assert (color? (rgb 0.1 0.2 0.3 0.4)))
  (test-assert (color? (rgb8 1 2 3 4)))
  (test-assert (color? (hsl 0.1 0.2 0.3 0.4)))
  (test-assert (not (color? '(1 2 3 4))))
  (test-assert (not (color? #(1 2 3 4))))
  (test-assert (not (color? #f32(1 2 3 4))))
  (test-assert (not (color? #u8(1 2 3 4)))))


(include "hsl-tests.scm")
(include "rgb-tests.scm")
(include "rgb8-tests.scm")
(include "array-tests.scm")
(include "convert-tests.scm")
(include "math-tests.scm")

(test-exit)
