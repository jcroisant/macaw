

(test-group "array?"
  (test-assert (array? (make-rgb-array 1 1)))
  (test-assert (array? (make-rgb8-array 1 1)))
  (test-assert (array? (make-hsl-array 1 1)))

  (test-assert (not (array? (rgb 1 2 3 4))))
  (test-assert (not (array? (rgb8 1 2 3 4))))
  (test-assert (not (array? (hsl 1 2 3 4))))

  (test-assert (not (array? '(1 2 3 4))))
  (test-assert (not (array? #(1 2 3 4))))
  (test-assert (not (array? #u8(1 2 3 4))))
  (test-assert (not (array? #f32(1 2 3 4))))
  (test-assert (not (array? (make-blob 4))))
  (test-assert (not (array? (make-locative (make-blob 4))))))


(test-group "array-ref"
  (let ((a (make-rgb-array 2 2)))
    (test (rgb-array-ref a 1 1)
          (array-ref a 1 1)))

  (let ((a (make-rgb8-array 2 2)))
    (test (rgb8-array-ref a 1 1)
          (array-ref a 1 1)))

  (let ((a (make-hsl-array 2 2)))
    (test (hsl-array-ref a 1 1)
          (array-ref a 1 1))))


(test-group "array-ref-pointer"
  (let ((a (make-rgb-array 2 2)))
    (test (rgb-array-ref-pointer a 1 1)
          (array-ref-pointer a 1 1)))

  (let ((a (make-rgb8-array 2 2)))
    (test (rgb8-array-ref-pointer a 1 1)
          (array-ref-pointer a 1 1)))

  (let ((a (make-hsl-array 2 2)))
    (test (hsl-array-ref-pointer a 1 1)
          (array-ref-pointer a 1 1))))


(test-group "array-for-each"
  (define data1 (make-f32vector (* 2 40)))
  (define data2 (make-f32vector (* 3 40)))
  (define data3 (make-u8vector (* 2 12)))
  (define a1 (rgb-array-at (make-locative data1) 2 2 40))
  (define a2 (hsl-array-at (make-locative data2) 2 3 40))
  (define a3 (rgb8-array-at (make-locative data3) 3 2 12))

  (let ((result '()))
    (array-for-each
     (lambda args (set! result (cons args result)))
     a1 a2 a3)

    (test "calls function with correct arguments in correct order"
          `((0 0
               ,(rgb-array-ref a1 0 0)
               ,(hsl-array-ref a2 0 0)
               ,(rgb8-array-ref a3 0 0))
            (1 0
               ,(rgb-array-ref a1 1 0)
               ,(hsl-array-ref a2 1 0)
               ,(rgb8-array-ref a3 1 0))
            (0 1
               ,(rgb-array-ref a1 0 1)
               ,(hsl-array-ref a2 0 1)
               ,(rgb8-array-ref a3 0 1))
            (1 1
               ,(rgb-array-ref a1 1 1)
               ,(hsl-array-ref a2 1 1)
               ,(rgb8-array-ref a3 1 1)))
          (reverse result))))


(test-group "array-for-each-pointer"
  (define data1 (make-f32vector (* 2 40)))
  (define data2 (make-f32vector (* 3 40)))
  (define data3 (make-u8vector (* 2 12)))
  (define a1 (rgb-array-at (make-locative data1) 2 2 40))
  (define a2 (hsl-array-at (make-locative data2) 2 3 40))
  (define a3 (rgb8-array-at (make-locative data3) 3 2 12))

  (let ((result '()))
    (array-for-each-pointer
     (lambda args (set! result (cons args result)))
     a1 a2 a3)

    (test "calls function with correct arguments in correct order"
          `((0 0
               ,(rgb-array-ref-pointer a1 0 0)
               ,(hsl-array-ref-pointer a2 0 0)
               ,(rgb8-array-ref-pointer a3 0 0))
            (1 0
               ,(rgb-array-ref-pointer a1 1 0)
               ,(hsl-array-ref-pointer a2 1 0)
               ,(rgb8-array-ref-pointer a3 1 0))
            (0 1
               ,(rgb-array-ref-pointer a1 0 1)
               ,(hsl-array-ref-pointer a2 0 1)
               ,(rgb8-array-ref-pointer a3 0 1))
            (1 1
               ,(rgb-array-ref-pointer a1 1 1)
               ,(hsl-array-ref-pointer a2 1 1)
               ,(rgb8-array-ref-pointer a3 1 1)))
          (reverse result))))
