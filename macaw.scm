;;;
;;; macaw:
;;; Efficient color types and math for CHICKEN Scheme.
;;;
;;; Copyright © 2020  John Croisant.
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;;
;;;  * Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;;
;;;  * Redistributions in binary form must reproduce the above
;;;    copyright notice, this list of conditions and the following
;;;    disclaimer in the documentation and/or other materials provided
;;;    with the distribution.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
;;; CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
;;; INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;;; DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
;;; BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
;;; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
;;; ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
;;; TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
;;; THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
;;; SUCH DAMAGE.


(module macaw ()
  (import scheme)
  (cond-expand
   (chicken-4
    (import chicken foreign)
    (use (only lolevel
               locative? make-locative locative->object
               pointer? pointer+ pointer-vector
               record-instance? record-instance-type)
         (only data-structures
               identity)
         srfi-4)
    (import-for-syntax (only extras sprintf)))
   (else
    (import (chicken base)
            (chicken blob)
            (chicken foreign)
            (only (chicken locative)
                  locative? make-locative locative->object)
            (only (chicken memory)
                  pointer? pointer+)
            (only (chicken memory representation)
                  record-instance? record-instance-type)
            (only (chicken module)
                  export)
            (chicken type)
            (srfi 4))
    (import-for-syntax (only (chicken format) sprintf))))

  #>
  #include "lib/macaw.c"
  <#

  (export color?)

  (: color? (any --> boolean))
  (define (color? x)
    (or (rgb? x) (rgb8? x) (hsl? x)))

  (define-type color (or (struct macaw:rgb)
                         (struct macaw:rgb8)
                         (struct macaw:hsl)))
  (declare (predicate (color? color)))

  (include "lib/helpers.scm")
  (include "lib/define-array-type.scm")
  (include "lib/define-color-type.scm")
  (include "lib/hsl.scm")
  (include "lib/rgb.scm")
  (include "lib/rgb8.scm")
  (include "lib/array.scm")
  (include "lib/convert.scm")
  (include "lib/math.scm")

  ) ;; end module
